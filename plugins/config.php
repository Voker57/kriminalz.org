<?php
require_once("shoo.php");
require_once("adodb5/session/adodb-session2.php");
require_once("adodb5/session/adodb-compress-gzip.php");
require_once("adodb5/session/adodb-encrypt-md5.php");
require_once("adodb5/adodb.inc.php");
require_once("plugins/cred.php");

ADOdb_Session::config('mysqli',$dbhost, $dbuser, $dbpassword, $dbdb);
ADOdb_Session::filter(new ADODB_Compress_Gzip());
ADOdb_Session::filter(new ADODB_Encrypt_MD5());
ADOdb_Session::lifetime(7*24*60*60);

session_set_cookie_params (7*24*60*60);
session_start();
$scheme = 'http';

if (get_magic_quotes_gpc()) {
    function stripslashes_deep($value)
    {
        $value = is_array($value) ?
                    array_map('stripslashes_deep', $value) :
                    stripslashes($value);

        return $value;
    }

    $_POST = array_map('stripslashes_deep', $_POST);
    $_GET = array_map('stripslashes_deep', $_GET);
    $_COOKIE = array_map('stripslashes_deep', $_COOKIE);
    $_REQUEST = array_map('stripslashes_deep', $_REQUEST);
}


$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
$db=NewADOConnection('mysqli');
$db->Connect($dbhost, $dbuser, $dbpassword, $dbdb) or die("db_error");

$hour=localtime(time(),1);
if(($hour['tm_hour']>17)&&($hour['tm_hour']<21))
{
 $cycle='Сумерки';
} elseif(($hour['tm_hour']>6)&&($hour['tm_hour']<18))
{
 $cycle='Шухер';
} else $cycle="Тьма";
$r=dirname($_SERVER['PHP_SELF']);
if($r="/") $r="";
$siteroot=$_SERVER['SERVER_NAME'];
$db->Execute("SET NAMES utf8");
//$db->debug=true;
?>