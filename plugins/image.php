<?php
require_once("shoo.php");
$nImgPrefix="images/upl";
$thumbwidth=200;
$thumbheight=150;
function nImageHandleUpload($tmpname)
{
	global $nImgPrefix,$thumbwidth, $thumbheight, $r, $siteroot;
	$size=getimagesize($tmpname);
	switch($size[2])
	{
		case IMAGETYPE_PNG: $imgtype="png"; break;
		case IMAGETYPE_GIF: $imgtype="gif"; break;
		case IMAGETYPE_JPEG: $imgtype="jpeg"; break;
		default: $fail=1;
	}
	$action="imagecreatefrom$imgtype";
	if(!isset($fail))
	{
		$image=$action($tmpname) or $fail=1;
		$time=mktime();
		$filename=md5($time);
		move_uploaded_file($tmpname,"$nImgPrefix/$filename.$imgtype");
		chmod("$nImgPrefix/$filename.$imgtype",0755);
		if(imagesx($image)>$thumbwidth)
		{
			$thumb=imagecreatetruecolor($thumbwidth,imagesy($image)*$thumbwidth/imagesx($image));
			imagecopyresampled($thumb,$image,0,0,0,0,$thumbwidth,imagesy($image)*$thumbwidth/imagesx($image),imagesx($image),imagesy($image));
		} else $thumb=$image;
		$action="image$imgtype";
		$action($thumb,"$nImgPrefix/thumbs/$filename.$imgtype");
	} else
	{
		return 0;
		$fail=1;
	};
	if(!isset($fail))
	{
		return "$filename.$imgtype";
	}
}
?>