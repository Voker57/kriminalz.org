<?php require("plugins/shoo.php");
require_once("plugins/config.php");
require_once("plugins/auth.php");
require_once("textile.php");
// Fuck markdown.
$textile=new Textile;

function &aExecute($query, $values=NULL)
{
	global $db;
	if($values!==NULL)
		return $db->Execute($query,$values);
	else
		return $db->Execute($query);
}

function &aGetAssoc($query, $values=NULL)
{
	global $db;
	if($values!==NULL)
		return $db->GetAssoc($query,$values);
	else
		return $db->GetAssoc($query);
}

function &aGetRow($query, $values=NULL)
{
	global $db;
	if($values!==NULL)
		return $db->GetRow($query,$values);
	else
		return $db->GetRow($query);
}

function &aGetCol($query, $values=NULL)
{
	global $db;
	if($values!==NULL)
		return $db->GetCol($query,$values);
	else
		return $db->GetCol($query);
}

function &aGetOne($query, $values=NULL)
{
	global $db;
	if($values!==NULL)
		return $db->GetOne($query,$values);
	else
		return $db->GetOne($query);
}

function &aGetAll($query, $values=NULL)
{
	global $db;
	if($values!==NULL)
		return $db->GetAll($query,$values);
	else
		return $db->GetAll($query);
}

function aFfectedRows()
{
	global $db;
	return $db->Affected_Rows();
}

function aBeginTrans()
{
	global $db;
	$db->BeginTrans();
}

function aStartTrans()
{
	global $db;
	$db->BeginTrans();
}

function aCommitTrans()
{
	global $db;
	$db->CommitTrans();
}

function aCompleteTrans()
{
	global $db;
	return $db->CompleteTrans();
}

function aRollbackTrans()
{
	global $db;
	$db->RollbackTrans();
}

function aFailTrans()
{
	global $db;
	$db->FailTrans();
}

function aHasFailedTrans()
{
	global $db;
	return $db->HasFailedTrans();
}

function aLastID($table)
{
	global $db;
	return ($db->GetOne("SELECT Auto_increment FROM information_schema.tables WHERE table_name=?",$table)-1);
}

function aUtoExecute($table, $arrFields, $mode, $where=false, $forceUpdate=true,$magicq=false)
{
	global $db;
	$db->AutoExecute($table, $arrFields, $mode, $where, $forceUpdate,$magicq);
}

function &aSum($query, $values=NULL)
{
	global $db;
	return array_sum($db->GetCol($query, $values));
}

function redir($base, $full=0)
{
	global $siteroot,$r,$scheme;
	if(!$full) $uri="$scheme://$siteroot/$base";
	else $uri=$base;
	echo "<div class='content'>
			Идет перенаправление <br />
			<a href='$uri'>быстрее!</a>
			<script type='text/javascript'>
			<!--
			window.location = '$uri'
			-->
			</script>
			</div>";
	unset($_POST);
	die("here goes...");
}

function err_inf($mess)
{
	$_SESSION['inf_err']=$_SESSION['inf_err'].$mess."<br />";
}

function debug($mess)
{
	echo "$mess <br />";
}

function redir_http($base)
{
	global $r, $siteroot;
	header("Location: http://$siteroot/$base");
}

function ms_esc(&$arg)
{
	$arg=preg_replace("/'/",'"',$arg);
}

function int_esc(&$arg)
{
	if(is_numeric($arg))
		$arg=round($arg);
	else $arg=0;
	return $arg;
}

function ht_esc(&$arg,$nomark=0)
{
	global $textile;
	if($nomark==0)
	{
		$arg=$textile->TextileRestricted($arg, 0, 0);
	} else
	{
		$arg=htmlspecialchars($arg, ENT_QUOTES, "UTF-8");
		$arg=preg_replace("/[\r\n]/","",$arg);
	}
}

function hard_esc(&$arg)
{
	global $textile;
	$arg=$textile->TextileRestricted($arg);
}
function nick_esc(&$arg)
{
	$arg=preg_replace("/</","[", $arg);
	$arg=preg_replace("/>/","]", $arg);
	return $arg;
}
// deprecated
function is_admin($nick)
{
	return ($nick == "[KoR]Voker57")||($nick == "[KoR]Ben-zin");
}
// deprecated
function is_green($nick)
{
	return ($nick == "Зеленка");
}

function nick()
{
	return $_SESSION['nick'];
}

function green()
{
	return is_green($_SESSION['nick']);
}

function admin()
{
	return is_admin($_SESSION['nick']);
}
function has_money($nick)
{
	if(is_green($nick)) $money=1;
	else
	{
		$res=aExecute("SELECT money FROM accounts WHERE name=?", array($nick));
		if($obb=$res->FetchRow())
		{
			if($obb['money']>0)
				$money=$obb['money'];
			else
			{
				$money=1;
				aExecute("UPDATE accounts SET money=1 WHERE name=?",array($nick));
			}
		} else return -1;
		$res->Close();
	}
	return $money;
}

function money()
{
	return has_money(nick());
}


function has_authority($nick)
{
	if(is_green($nick)) $authority=1;
	else
	{
		$res=aExecute("SELECT authority FROM accounts WHERE name=?",array($nick));
		if($obb=$res->FetchRow())
			$authority=$obb['authority'];
		else $authority=-1;
		$res->Close;
	}
	return $authority;
}

function authority()
{
	return has_authority(nick());
}
/*function in_array($needle, $haystack)
{

	while(list($key, $value) = each($haystack))
	{
		if(!strcasecmp($value, $needle))
			return(TRUE);
	}

	return(FALSE);
}*/
function sort_caves($a,$b)
{
	if ($a['priority'] == $b['priority']) return 0;
	return ($a['priority'] > $b['priority']) ? -1 : 1;
}

function take_money($nick, $amount)
{
//	$amount=round($amount);
	int_esc($amount);
	if(has_money($nick)>=$amount)
	{
		aExecute("UPDATE accounts SET money=money-? WHERE name=?",array($amount,$nick));
		return 1;
	}
	else return 0;
}

function give_money($nick, $amount)
{
//	$amount=round($amount);
	int_esc($amount);
	aExecute("UPDATE accounts SET money=money+$amount WHERE name='$nick'");
	return 1;
}

?>