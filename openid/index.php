<?php
// IF YOU HAVE NOT DONE SO, PLEASE READ THE README FILE FOR DIRECTIONS!!!

/**
 * phpMyID - A standalone, single user, OpenID Identity Provider
 *
 * @package phpMyID
 * @author CJ Niemira <siege (at) siege (dot) org>
 * @copyright 2006-2008
 * @license http://www.gnu.org/licenses/gpl.html GNU Public License
 * @url http://siege.org/projects/phpMyID
 * @version 2
 */


/**
 * User profile
 * @name $profile
 * @global array $GLOBALS['profile']
 */
$GLOBALS['profile'] = array(
	# Basic Config - Required
	'auth_username'	=> 	'Voker57',
	'auth_password' =>	'ce4791c2691f9b071a2044e4a1905f8e',

	# Optional Config - Please see README before setting these
#	'microid'	=>	array('user@site.com', 'http://delegator.url'),
#	'pavatar'	=>	'http://your.site.com/path/pavatar.img',

	# Advanced Config - Please see README before setting these
#	'allow_gmp'	=>true,
#	'allow_test'	=> 	false,
	'auth_realm'	=>	'NBL',
#	'force_bigmath'	=>	false,
	'idp_url'	=>	'http://id.nblast.org',
#	'lifetime'	=>	1440,
#	'paranoid'	=>	false, # EXPERIMENTAL

	# Debug Config - Please see README before setting these
#	'debug'		=>	false,
#	'logfile'	=>	'/tmp/phpMyID.debug.log',
);

/**
 * Simple Registration Extension
 * @name $sreg
 * @global array $GLOBALS['sreg']
 */
$GLOBALS['sreg'] = array (
	'nickname'		=> 'Voker57',
	'email'			=> 'voker57@gmail.com',
	'fullname'		=> 'Voker57',
	'dob'			=> '1989-11-23',
	'gender'		=> 'M',
#	'postcode'		=> '22000',
	'country'		=> 'RU',
	'language'		=> 'ru',
	'timezone'		=> 'Russia/Moscow'
);

require('MyID.php');
?>
