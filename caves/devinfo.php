<?php require("shoo.php");
addcave('devinfo', "Инф. инфо", "dev", 0);
function cave_devinfo()
{ global $siteroot, $r;
?>
<div class='content'>
<h1> Инф. инфо </h1>
<p>
Чтобы узнать характеристики вангера через Интернет (сейчас это <b>Авторитет</b> и <b>Бибы</b>), нужно послать http-запрос на адрес <?php echo "http://$siteroot"; ?>/info/<b>ник</b>/<b>свойство</b> , где <b>свойство</b>=<b>money</b> или <b>authority</b>. В конце можно по вкусу добавить слэш.
</p>
<p>
Сервер ответит на это строкой, содержащей положительное число (или -1 в случае отсутствующего вангера; -2 в случае неправильного запроса)
</p>
<p> Пример: запрос — "<?php echo "http://$siteroot"; ?>/info/[KoR]Voker57/money", ответ — "109"</p>
<p> Код на PHP, реализующий запрос текущего количества денег у вангера:
<pre>
function get_beebs($nick)
{
	return file_get_contents("<?php echo "http://$siteroot"; ?>/info/$nick/money");
}
</pre></p>
</div>
<?php }
?>
