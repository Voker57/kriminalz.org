<?php require("shoo.php");
addcave('coop', 'Кооператив');
 function cave_coop()
{
?>

<div class='content'>
<h2 class='text'>
КоОпеРатИв
</h2>
	<div class='text'>
	Здесь собираются все катафальщики, которым что-то нужно, а также те, кто хочет заработать пару кучек бибов.
	</div>
<?php
	// do the requests
	if(isset($_POST['success']))
	{
		$res=aExecute("SELECT * FROM tabootasks WHERE id=? AND assigned!='' AND expired=0 AND nick=?",array($_POST['success'],$_SESSION['nick']));
		if($obb=$res->FetchRow())
		{
			aExecute("UPDATE accounts SET money=money+?, authority=authority+10 WHERE name=?",array($obb['price'],$obb['assigned']));
		//	mysql_unbuffered_query("UPDATE accounts SET authority=authority+2 WHERE name='$obb[nick]'"); exploitable!
			aExecute("DELETE FROM tabootasks WHERE id=?",array($obb['id']));
		}
		else err_inf("Попытка нарушения структуры реальности. Пресечено");
		$res->Close();
		redir("cave/coop");
	}
	elseif((isset($_POST['take']))&&(!green()))
	{
		int_esc($_POST['take']);
		$res=aExecute("SELECT * FROM tabootasks WHERE id=? AND assigned='' AND expired=0",array($_POST['take']));
		if($obb=$res->FetchRow())
		{
			if($obb['nick']!=nick())
			{
				aExecute("UPDATE tabootasks SET assigned=? WHERE id=?",array($_SESSION['nick'],$obb['id']));
			} else err_inf("Предотвращено самозамыкание.");
		} else err_inf("Попытка нарушения структуры реальности. Пресечено");

		$res->Close();
		redir("cave/coop");
	}
	elseif(($_POST['action']=='add')&&(!green())&&(ereg("^[0-9]*$",$_POST['price'])))
	{
		ht_esc($_POST['title'],1);
		ht_esc($_POST['summary']);
		ht_esc($_POST['time'],1);
		int_esc($_POST['price']);
		$res=aExecute("SELECT * FROM accounts WHERE name=? AND money>=?",array($_SESSION['nick'],$_POST['price']));
		if($res->RecordCount()==1)
		{
			aExecute("INSERT INTO tabootasks VALUES(NULL,?,'',0,?,?,?,?,NULL)",array($_SESSION['nick'],$_POST['title'],$_POST['time'],$_POST['summary'],$_POST['price']));
			aExecute("UPDATE accounts SET money=money-? WHERE name=?",array($_POST['price'],$_SESSION['nick']));
		}
		else err_inf("Недостаточны обьемы биомассы!");
		$res->Close();
		redir("cave/coop");
	}elseif(isset($_POST['expire']))
	{
		int_esc($_POST['expire']);
		$res=aExecute("SELECT * FROM tabootasks WHERE id=? AND nick=?",array($_POST['expire'],$_SESSION['nick']));
		if($obb=$res->FetchRow())
		{
			aExecute("UPDATE tabootasks SET expired=1 WHERE id=?",array($obb['id']));
			if(isset($obb['assigned']))
			{
				aExecute("UPDATE accounts SET authority=authority-10 WHERE name=?",array($obb['assigned']));
				aExecute("UPDATE accounts SET authority=0 WHERE authority<0");
			} else err_inf("Отсутствует целевой обьект операции");
		} else err_inf("Попытка нарушения структуры реальности. Пресечено");
		$res->Close();
	redir("cave/coop");
	}
	if(isset($_POST['delete']))
	{
		int_esc($_POST['delete']);
		if(!admin())
		{
			$res=aExecute("SELECT * FROM tabootasks WHERE id=? AND nick=? AND expired=1",array($_POST['delete'],$_SESSION['nick']));
			if($res->RecordCount()==1) $del_id=$_POST['delete'];
		} else
		{
			$del_id=$_POST['delete'];
			$res=aExecute("SELECT * FROM tabootasks WHERE id=?",array($_POST['delete']));
		}
		if(isset($del_id))
		{
			$obb=$res->FetchRow();
			aExecute("UPDATE accounts SET money=money+? WHERE name=?",array($obb['price'],$obb['nick']));
			aExecute("DELETE FROM tabootasks WHERE id=?",array($_POST['delete']));
		} else err_inf("Попытка нарушения структуры реальности. Пресечено");
		$res->Close();
		redir("cave/coop");
	};
	//mysql_query('INSERT INTO tabootasks VALUES(NULL,"[KoR]Voker57", "[KoR]Voker57", 0, "Табутаски", "к старости", "Сделай систему табутасков", 256)');
	$res=aExecute("SELECT * FROM tabootasks WHERE assigned='' AND expired=0");
	if ($res->RecordCount()>0)
	{
		echo "<h3 class='text'>
				Невзятые табутаски
				</h3>";
		while ($obb=$res->FetchRow())
		{
			echo "
			<div class='tabootask'>
			<span class='tabootext'>
			<b>Вангер:</b> $obb[nick]
			<br />
			<b>Табутаск:</b> $obb[title]
			<br />
			<b>Время:</b> $obb[time]
			<br />
			<b>Подробности:</b> $obb[summary]
			<br />
			<b>Цена:</b> $obb[price] <img src='/images/beeb.png' alt='beebs='>
			";
			if((!green())&&($_SESSION['nick']!=$obb['nick']))
			{
				echo "<form action='/inside' method='post'>
					<input type='hidden' name='take' value='$obb[id]'>
					<input type='submit' value='Взять табутаск'>
					</form>";
			}
			if(admin()) echo "<form action='/inside' method='post'> <input type='hidden' name='delete' value='$obb[id]'> <input type='submit' value='Удалить табутаск'></form>";
			echo "
			</span>
			</div>";
		};
	};
	$res->Close();
	// assigned to you
	$res=aExecute("SELECT * FROM tabootasks WHERE assigned=? AND expired=0",array($_SESSION['nick']));
	if ($res->RecordCount()>0)
	{
		echo "<h3 class='text'>
				Твоя работа
			</h3>";
		while ($obb=$res->FetchRow())
			echo "
					<div class='tabooprogress'>
					<span class='tabootext'>
					<b>Вангер:</b> $obb[nick]
					<br />
					<b>Табутаск:</b> $obb[title]
					<br />
					<b>Время:</b> $obb[time]
					<br />
					<b>Подробности:</b> $obb[summary]
					<br />
					<b>Цена:</b> $obb[price] <img src='/images/beeb.png' alt='beebs='>
					</span>
					</div>";
	};
	$res->Close();
	// failed ones
	$res=aExecute("SELECT * FROM tabootasks WHERE assigned=? AND expired=1",array($_SESSION['nick']));
	if ($res->RecordCount()>0)
	{
		echo "<h3 class='text'>
				Немой укор
				</h3>";
		while ($obb=$res->FetchRow())
			echo "
					<div class='taboofailed'>
					<span class='tabootext'>
					<b>Вангер:</b> $obb[nick]
					<br />
					<b>Табутаск:</b> $obb[title]
					<br />
					<b>Время:</b> $obb[time]
					<br />
					<b>Подробности:</b> $obb[summary]
					<br />
					<b>Цена:</b> $obb[price] <img src='/images/beeb.png' alt='beebs='>
					</span>
					</div>";
	};
	$res->Close();
	$res=aExecute("SELECT * FROM tabootasks WHERE nick=?",array($_SESSION['nick']));
	if($res->RecordCount()>0)
	{
		echo "<h3 class='text'>
				Твои табутаски
			</h3>";

		while ($obb=$res->FetchRow())
			{
				echo "<div class='";
				if(($obb['assigned']=="")&&($obb['expired']==0)) echo "tabootask";
				elseif($obb['expired']==1) echo "taboofailed";
				else echo "tabooprogress";


				echo "'>
				<span class='tabootext'>
				<b>Табутаск:</b> $obb[title]
				<br />
				<b>Время:</b> $obb[time]<br />
				<b>Подробности:</b> $obb[summary]
				<br />
				<b>Цена:</b> $obb[price] <img src='/images/beeb.png' alt='beebs='>";
				if(($obb['expired']==0)&&($obb['assigned']!=""))
					echo "<form action='/inside' method='post'> <input type='hidden' name='success' value='$obb[id]'> <input type='submit' value='Табутаск исполнен'></form>";
				if($obb['expired']==0)
					echo "<form action='/inside' method='post'> <input type='hidden' name='expire' value='$obb[id]'> <input type='submit' value='Табутаск просрочен'></form>";
				if(($obb['expired']==1)||(admin()))
					echo "<form action='/inside' method='post'> <input type='hidden' name='delete' value='$obb[id]'> <input type='submit' value='Удалить табутаск'></form>";
				echo "</span></div>";
			};

	};
	// other
	$res=aExecute("SELECT * FROM tabootasks WHERE assigned!=? AND nick!=? AND assigned!=''",array($_SESSION['nick'],$_SESSION['nick']));
	if ($res->RecordCount()>0)
{
		echo "<h3 class='text'>
				Эти табутаски не имеют к тебе никакого отношения
				</h3>";
		while ($obb=$res->FetchRow())
		{
				echo "
					<div class='";
				if(($obb['assigned']=="")&&($obb['expired']==0)) echo "tabootask";
				elseif($obb['expired']==1) echo "taboofailed";
				else echo "tabooprogress";
				echo "'>
					<span class='tabootext'>
					<b>Вангер:</b> $obb[nick]
					<br />
					<b>Взято:</b> $obb[assigned]
					<br />
					<b>Табутаск:</b> $obb[title]
					<br />
					<b>Время:</b> $obb[time]
					<br />
					<b>Подробности:</b> $obb[summary]
					<br />
					<b>Цена:</b> $obb[price] <img src='/images/beeb.png' alt='beebs='>
					</span>
					</div>";
		}
};
	$res->Close();
	if(!green())
	echo '
	<h3 class="text">
	Новый табутаск
	</h3>
	<div class="text">
		<form action="/cave/coop" method="post">
		<input type="hidden" name="action" value="add">
		<label for="title">Заголовок</label>: <input type="text" name="title"> <br />
		<label for="time">Временные рамки</label>: <input type="text" name="time"> <br />
		<label for="price">Цена (только цифры)</label>: <input type="text" name="price"> <br />
		<label for="summary">Суть</label>: <textarea name="summary" rows=5 cols=80></textarea> <br />
		<input type=submit value="Панеслася">
		</form>
	</div>';
?>
</div>
<?php }; ?>